#!/bin/bash
# This should run daily as cron entry
export DATE=`date "+%Y%m%d"`

# Take a backup of a mysql server
. /home/serveruser/.sqlcredentials
docker exec mysql /bin/sh -c "mysqldump -u$SQLUSER -p$SQLPASS --single-transaction --routines --triggers --databases vanilla > /var/lib/mysql/backup/${DATE}_backup_vanilla.sql"

# Mount external filesystem & copy backup
BACKUP_DIR=`docker container inspect mysql | grep -i source | awk '{print $2}' | sed -r 's/[",]//g'`/backup
MOUNT_DIR=/mnt/x
EXTERNAL_BACKUP_DIR=/mnt/x/VanillaBackups

# Check if already mounted and mount if needed
if ! grep -qs '/mnt/x ' /proc/mounts; then
    mount -t cifs -o credentials=/home/serveruser/smbclient.conf,vers=2.0 //10.10.1.3/Engineering $MOUNT_DIR
fi
cp $BACKUP_DIR/${DATE}_backup_vanilla.sql $EXTERNAL_BACKUP_DIR

# Cleanup old local backups ( older than 30 days)
find $BACKUP_DIR -mtime +30 -name "*vanilla.sql" -delete

# Unmout filesystem
umount $MOUNT_DIR